<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 7-2-2018
 * Time: 12:21
 */

class Language
{

    private $available_lang = array('en', 'nl');
    private $default = 'nl';

    public function registerLanguage()
    {
        $func = new Functions();
        $func->startSession();
        $_SESSION['lang'] =$this->default;

        if (isset($_GET['lang']) && $_GET['lang'] != ''){
            if (in_array($_GET['lang'], $this->available_lang)){
                $_SESSION['lang'] = $_GET['lang'];
            }
        }

        return include('languages/'.$_SESSION['lang'].'/lang.'.$_SESSION['lang'].'.php');
    }

}