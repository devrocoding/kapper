<?php
/**
 * Created by PhpStorm.
 * User: JusJus
 * Date: 7-2-2018
 * Time: 12:21
 */

class Functions
{

    public function startSession()
    {
        if (session_status() == PHP_SESSION_NONE) {
            session_start();
        }
    }

}